FROM openjdk:8-jre
FROM maven:3-jdk-8

WORKDIR /app
  

 
EXPOSE 8080       

CMD ["java", "-war", "jsf.war"]