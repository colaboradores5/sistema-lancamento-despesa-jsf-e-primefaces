package TesteContrutores;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;
import com.sistema.erp.model.Despesa;
import com.sistema.erp.model.Tipo;
import com.sistema.erp.repository.despesaRepository;

public class TesteConstrutores implements Serializable{

	private static final long serialVersionUID = 1L;

	
	despesaRepository cursosRepository = new despesaRepository();
	
	@Test
	public void testContrutores() {
		Despesa despesa = new Despesa();
		despesa.setDataLancamento(LocalDate.now());
		despesa.setDataVencimento(LocalDate.now());
		despesa.setDescricao("Faculdade");
		despesa.setTipo(Tipo.GRADUACAO);
		despesa.setValor(BigDecimal.valueOf(50));
		despesa.setId(100l);
	}
	
}
