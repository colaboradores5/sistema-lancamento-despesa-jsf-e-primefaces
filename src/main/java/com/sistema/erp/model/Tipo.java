package com.sistema.erp.model;

public enum Tipo {

	GRADUACAO("Graduação"), 
	POS("Pós-Graduação"),
	CURSOS("Curso de programação"),
	INTERNET("Plano de internet");
	
	private String descricao;

	Tipo(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
