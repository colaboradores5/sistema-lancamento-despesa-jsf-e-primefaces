package com.sistema.erp.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.sistema.erp.model.Despesa;
import com.sistema.erp.repository.despesaRepository;
import com.sistema.erp.util.Transacional;

public class DespesaService implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private despesaRepository cursosRepository;
	
	@Transacional
	public void adicionar(Despesa cursos) {
		cursosRepository.adicionar(cursos);
	}
	
	@Transacional
	public void remover(Despesa cursos) {
		cursosRepository.remover(cursos);
	}
	
}
