package com.sistema.erp.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.sistema.erp.model.Despesa;

/*
 * CAMADA DE PERSISTENCIA DO PROJETO
 */

public class despesaRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public despesaRepository() { }
	
	public despesaRepository(EntityManager manager) {
		this.manager = manager;
	}
	
	public Despesa buscarPorId(Long id) {
		return manager.find(Despesa.class, id);
	}
	
	public List<Despesa> buscarPorNome(String descricao){
		TypedQuery<Despesa> query = manager.createQuery("from Despesa where descricao like :descricao", Despesa.class);
		query.setParameter("descricao", descricao + "%");
		return query.getResultList();
	}
	
	public List<Despesa> listar(){
		return manager.createQuery("from Despesa", Despesa.class).getResultList();
	}
	
	public Despesa adicionar( Despesa cursos) {
		return manager.merge(cursos);
	}
	
	public void remover(Despesa cursos) {
		cursos = buscarPorId(cursos.getId());
		manager.remove(cursos);
	}
	
}
