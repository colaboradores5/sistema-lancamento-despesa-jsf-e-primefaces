package com.sistema.erp.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.sistema.erp.model.TipoDespesa;

public class TipoDespesaRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public TipoDespesaRepository() { }
	
	public TipoDespesaRepository(EntityManager manager) { 
		this.manager = manager;
	}
	
	public List<TipoDespesa> busrcaPorNome(String nome){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<TipoDespesa> query = builder.createQuery(TipoDespesa.class);
		
		Root<TipoDespesa> root = query.from(TipoDespesa.class);
		
		query.select(root);
		
		query.where(builder.like(root.get("descricao"), nome + "%"));
		
		TypedQuery<TipoDespesa> typedQuery = manager.createQuery(query);
		
		return typedQuery.getResultList();
	}
	
	public List<TipoDespesa> listar(){
		TypedQuery<TipoDespesa> query = manager.createQuery("from TBL_TIPO_DESPESA", TipoDespesa.class);
		return query.getResultList();
	}
	
}
