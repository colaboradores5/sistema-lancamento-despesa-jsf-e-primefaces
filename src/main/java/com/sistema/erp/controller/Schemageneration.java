package com.sistema.erp.controller;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.sistema.erp.model.Despesa;
public class Schemageneration {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("sistemaPU");
		
		EntityManager em = emf.createEntityManager();
		
		List<Despesa> lista = em.createQuery("from Despesa", Despesa.class)
				.getResultList();  
		 
		System.out.println(lista);
		
		em.close();
		emf.close();
		
		System.out.println("Nova funcionalidade001...");
		System.out.println("Nova funcionalidade002...");
		
		System.out.println("Nova funcionalidade005...");
		System.out.println("Nova funcionalidade006...");
		
		System.out.println("Nova funcionalidade007...");
		System.out.println("Nova funcionalidade008...");
	}
	
}
