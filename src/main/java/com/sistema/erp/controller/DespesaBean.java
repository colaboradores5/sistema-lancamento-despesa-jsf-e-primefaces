package com.sistema.erp.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.view.ViewScoped; //Ao carregar a página é criada uma instancia
import javax.inject.Inject;
import javax.inject.Named;

import com.sistema.erp.model.Despesa;
import com.sistema.erp.model.Tipo;
import com.sistema.erp.repository.despesaRepository;
import com.sistema.erp.service.DespesaService;
import com.sistema.erp.util.Mensagem;

/*
 * ESSA CLASSE SERÁ ACESSÍVEL PELAS PÁGINAS XHMTL POR CONTA DO @NAMED..
 * */

@Named
@ViewScoped
public class DespesaBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private despesaRepository despesaRepository;
	
	@Inject
	private DespesaService despesaService;
	
	private List<Despesa> listaDespesa;
	
	private String busca;
	
	@Inject
	private Mensagem messages;
	
	private Despesa despesa;
	
	public void prepararNovaDespesa () {
		despesa = new Despesa();
	}
	
	public void remover() {
		despesaService.remover(despesa);
		despesa = null;
		atualizarTabela();
		messages.info("Despesa removida com sucesso!");
	}
	
	public boolean verificaSeHouvePesquisa() {
		return busca != null && !"".equals(busca);
	}
	
	public void adicionar() {
		despesaService.adicionar(despesa);
		
		atualizarTabela();
		
		messages.info("Despesa adicionada na base de dados com sucesso!");
	}
	
	public void buscar() {
		listaDespesa = despesaRepository.buscarPorNome(busca);
		
		if(listaDespesa.isEmpty()) {
			messages.info("Não existe uma despesa com essa descrição.");
		}
	}
	
	public void listar() {
		listaDespesa = despesaRepository.listar();
	}
	
	public void atualizarTabela() {
		if(verificaSeHouvePesquisa()) {
			listar();
		}
	}
	
	public String logar() {
		return "login?faces-redirect=true";
	}
	
	public List<Despesa> getListaDespesa() {
		return listaDespesa;
	} 
	
	public static String fomatDateResultSqlSimple(Object object) {
	    return new SimpleDateFormat("dd/MM/yyyy").format(object);
	}
	
	public String getBusca() {
		return busca;
	} 
	
	public void setBusca(String busca) {
		this.busca = busca;
	}
	
	public Tipo[] getTipo() {
		return Tipo.values();
	}
	
	public Despesa getDespesa() {
		return despesa;
	}
	
	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}
	
	public boolean isDespesaSelecionada() {
		return despesa != null && despesa.getId() != null;
	}
	
}
